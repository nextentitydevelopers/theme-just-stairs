<?php

/**
 * @file
 * Template overrides as well as (pre-)process and alter hooks for the
 * Just Stairs theme.
 */

function just_stairs_preprocess_html(&$variables) {
  drupal_add_css('//netdna.bootstrapcdn.com/font-awesome/4.0.1/css/font-awesome.css', array('type' => 'external'));
}
