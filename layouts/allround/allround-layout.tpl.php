<div<?php print $attributes; ?>>
  <header class="l-header" role="banner">
    <div class="l-branding">
      <?php if ($logo): ?>
        <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" class="site-logo"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a>
      <?php endif; ?>

      <?php if ($site_name || $site_slogan): ?>
        <?php if ($site_name): ?>
          <h1 class="site-name">
            <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
          </h1>
        <?php endif; ?>

        <?php if ($site_slogan): ?>
          <h2 class="site-slogan"><?php print $site_slogan; ?></h2>
        <?php endif; ?>
      <?php endif; ?>

      <?php print render($page['branding']); ?>
    </div>

    <?php print render($page['header']); ?>
    <?php print render($page['navigation']); ?>
  </header>

  <div class="l-hero-wrap">
    <div class="l-hero">
      <div class="l-hero-inner">
        <?php print render($page['hero']); ?>
      </div>
      <div class="l-prefix">
        <div class="l-prefix-first">
          <?php print render($page['prefix_first']); ?>
        </div>
        <div class="l-prefix-second">
          <?php print render($page['prefix_second']); ?>
        </div>
        <div class="l-prefix-third">
          <?php print render($page['prefix_third']); ?>
        </div>
      </div>
      <div class="l-title">
        <div class="l-title-inner">
          <?php print render($title_prefix); ?>
          <?php if ($title): ?>
            <h1><?php print $title; ?></h1>
          <?php endif; ?>
          <?php print render($title_suffix); ?>
        </div>
        <div class="header-cta">
          <a href="#" class="button button-rounded button-action"><i class="fa fa-envelope">&nbsp;</i> Enquire Now</a>
        </div>
      </div>
    </div>
  </div>

  <div class="l-main">
    <div class="l-content" role="main">
        <?php print render($page['highlighted']); ?>
        <?php // print $breadcrumb; ?>
        <a id="main-content"></a>
        <?php print $messages; ?>
        <?php print render($tabs); ?>
        <?php print render($page['help']); ?>
        <?php if ($action_links): ?>
          <ul class="action-links"><?php print render($action_links); ?></ul>
        <?php endif; ?>
      <?php print render($page['content']); ?>
      <?php print $feed_icons; ?>
    </div>

    <?php print render($page['sidebar_first']); ?>
    <?php print render($page['sidebar_second']); ?>
  </div>

  <div class="l-post-main">
    <?php print render($page['post_content']); ?>
  </div>

  <div class="l-footer-wrap">
    <footer class="l-footer" role="contentinfo">
      <div class="l-footer-first">
        <?php print render($page['footer_first']); ?>
      </div>
      <div class="l-footer-second">
        <?php print render($page['footer_second']); ?>
      </div>
      <div class="l-footer-third">
        <?php print render($page['footer_third']); ?>
      </div>
      <div class="l-footer-forth">
        <?php print render($page['footer_forth']); ?>
      </div>
      <div class="l-footer-wide">
        <?php print render($page['footer']); ?>
      </div>
    </footer>
  </div>

  <div class="l-post-footer-wrap">
    <div class="l-post-footer">
      <?php print render($page['post_footer']); ?>
    </div>
  </div>

</div>
