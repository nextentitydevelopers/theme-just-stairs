name = AllRound
description = A all-round 3 column layout demonstrating mobile first and Susy, based on simple layout. More regions for general sites available.
preview = preview.png
template = allround-layout

; Regions
regions[branding]       = Branding
regions[header]         = Header
regions[navigation]     = Navigation bar
regions[hero]           = Hero
regions[prefix_first]   = Prefix First
regions[prefix_second]  = Prefix Second
regions[prefix_third]   = Prefix Third
regions[highlighted]    = Highlighted
regions[help]           = Help
regions[content]        = Content
regions[post_content]   = Post Content
regions[postfix]        = Postfix
regions[sidebar_first]  = First sidebar
regions[sidebar_second] = Second sidebar
regions[footer_first]   = Footer First
regions[footer_second]  = Footer Second
regions[footer_third]   = Footer Third
regions[footer_forth]   = Footer Forth
regions[footer]         = Footer
regions[post_footer]    = Post Footer

; Stylesheets
stylesheets[all][] = css/layouts/allround/allround.layout.css
